# Xenoverse: Per Aspera Ad Astra - Source Code Repository

## Disclaimer
Hello everyone and welcome to Xenoverse: Per Aspera Ad Astra's source code repository. 
Please understand that this repository is not meant to be maintained and you should not expect to find documentation on the contents of this repository.
Moreover, by using or referring to this repository you're acknowledging and agreeing to the guidelines at this link:
[Xenoverse: Per Aspera Ad Astra Code Access and Mods Guidelines](https://gitlab.com/weedleteam/Xenoverse-Source/-/blob/main/CodeAndModsGuidelines.md)

Useful links:
[Beehive Studios' Discord Server](https://gitlab.com/weedleteam/Xenoverse-Source/-/blob/main/CodeAndModsGuidelines.md) - (**Please do not discuss mods development there, as it's a general-purpose server**. If you do, you will be banned.)